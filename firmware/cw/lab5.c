#include "hal.h"
#include <stdint.h>
#include <stdlib.h>

#include "simpleserial.h"

unsigned char correct_pass[8];

uint8_t get_pass(uint8_t* pass, uint8_t len)
{
    uint8_t idx;

    trigger_high();

    for ( idx = 0 ; idx < 8 ; idx ++ )
        correct_pass[idx] = pass[idx];

    trigger_low();

    simpleserial_put('r', len, correct_pass);
    return 0x00;
}

uint8_t check_pass(uint8_t* pass, uint8_t len)
{
    uint8_t idx;
    uint8_t ok = 1;

    trigger_high();

    for ( idx = 0 ; idx < 8 ; idx ++ )
        if (correct_pass[idx] != pass[idx]) {
            ok = 0;
            break;
        }

    trigger_low();

    simpleserial_put('r', 1, &ok);
    return 0x00;
}

uint8_t check_pass_secure(uint8_t* pass, uint8_t len)
{
    uint8_t idx;
    uint8_t ok = 0;

    trigger_high();

    for ( idx = 0 ; idx < 8 ; idx ++ )
        ok |= correct_pass[idx] ^ pass[idx];

    trigger_low();

    if ( ok == 0 )
        ok = 1;
    else
        ok = 0;
    simpleserial_put('r', 1, &ok);
    return 0x00;
}

int main(void)
{
    platform_init();
    init_uart();
    trigger_setup();

    simpleserial_init();
    simpleserial_addcmd('p', 8, get_pass);
    simpleserial_addcmd('c', 8, check_pass);
    simpleserial_addcmd('s', 8, check_pass_secure);

    while(1)
        simpleserial_get();
}
